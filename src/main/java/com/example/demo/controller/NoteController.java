package com.example.demo.controller;

import com.example.demo.entities.Note;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.repositories.NoteRepo;
import com.example.demo.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NoteController {

    NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @PostMapping("/notes/add")
    public ResponseEntity createNote(@RequestBody Note note){
        return ResponseEntity.ok(noteService.save(note));
    }
    boolean first = true;
    @GetMapping("/notes")
    public ResponseEntity<List<Note>> fetchAll(){

        //init note 3 object
        if(first) {
            first = false;
            noteService.autoCreateNote();
        }

        return ResponseEntity.ok(noteService.fetchAll());
    }

    @GetMapping("/note/{id}")
    public ResponseEntity<Note> findById(@PathVariable Long id){
        return ResponseEntity.ok(noteService.findById(id).get());
    }

    @Autowired
    NoteRepo noteRepo;

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNoteById(@PathVariable Long id) throws ResourceNotFoundException{
        Note note = noteRepo.findById(id).orElseThrow(()-> new ResourceNotFoundException("Note","id",id));
        noteService.delete(note);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/notes/{id}")
    public ResponseEntity updateNoteById(@PathVariable Long id,@RequestBody Note note){
        return ResponseEntity.ok(noteService.updateNoteById(id,note));
    }
}
