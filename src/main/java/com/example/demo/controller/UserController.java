package com.example.demo.controller;

import com.example.demo.entities.onetoone.User;
import com.example.demo.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    UserRepo userRepo;

    @GetMapping("/users")
    public Object findAllUser(){
        return userRepo.findAll();
    }

    @PostMapping("/users/add")
    public User createUser(@RequestBody User user){
        return userRepo.save(user);
    }
}
