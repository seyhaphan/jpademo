package com.example.demo.controller;

import com.example.demo.entities.manytoone.EntityA;
import com.example.demo.entities.onetomany.User;
import com.example.demo.repositories.EntityRepo;
import com.example.demo.repositories.PhoneNumberRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PhoneNumberController {

   @Autowired
   PhoneNumberRepo phoneNumberRepo;

   @Autowired
   EntityRepo entityRepo;

   @GetMapping("/many-to-one")
   public List<EntityA> list(){
       return entityRepo.findAll();
   }

   @GetMapping("/one-to-many")
   public List<User> findAllPhoneNumber(){
       return phoneNumberRepo.findAll();
   }

}
