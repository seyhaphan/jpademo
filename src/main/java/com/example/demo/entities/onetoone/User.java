package com.example.demo.entities.onetoone;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String name;

    @OneToOne(cascade = CascadeType.PERSIST)
    //    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @Builder
    public User(String name, Address address) {
        this.name = name;
        this.address = address;
    }
}
