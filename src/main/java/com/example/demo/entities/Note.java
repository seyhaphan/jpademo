package com.example.demo.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity(name="notes")
@Getter
@Setter
public class Note{
    @Id
    @GeneratedValue()
    Long id;

    String message;

    public Note(String message) {
        this.message = message;
    }

    public Note() {

    }
}
