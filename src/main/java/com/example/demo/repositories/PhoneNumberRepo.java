package com.example.demo.repositories;

import com.example.demo.entities.onetomany.PhoneNumber;
import com.example.demo.entities.onetomany.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepo extends JpaRepository<User, Long> {
}
