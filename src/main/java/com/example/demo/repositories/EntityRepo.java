package com.example.demo.repositories;

import com.example.demo.entities.manytoone.EntityA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EntityRepo extends JpaRepository<EntityA,Long> {

    List<Object> findAllById(Long id);

}
