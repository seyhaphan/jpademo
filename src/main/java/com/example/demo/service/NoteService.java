package com.example.demo.service;

import com.example.demo.entities.Note;
import com.example.demo.repositories.NoteRepo;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NoteService {

    @PersistenceContext
    public EntityManager entityManager;

   public void autoCreateNote(){
       for (int i = 1; i <= 3 ; i++){
           entityManager.persist(new Note("note " + (i)));
       }
   }

    NoteRepo noteRepo;

    public NoteService(NoteRepo noteRepo) {
        this.noteRepo = noteRepo;
    }

    public List<Note> fetchAll(){
        return noteRepo.findAll();
    }
  
    public Optional<Note> findById(Long id) {
        return noteRepo.findById(id);
    }

    public Note save(Note note) {
        return noteRepo.save(note);
    }

    public void delete(Note note) {
        noteRepo.delete(note);
    }

    public Note updateNoteById(Long id, Note updateNote) {
        Note oldNote = noteRepo.findById(id).get();
        oldNote.setMessage(updateNote.getMessage());
       return noteRepo.save(oldNote);
    }
}
